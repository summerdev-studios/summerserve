const io = require('@pm2/io');
const http = require('http');
const net = require('net');
var url=require('url');
const fs = require('fs').promises;
const logger = require("fancy-log");

// pm2.io monitoring
const apiTotalReqs = io.counter({
    name: 'Total Request Count',
    id: 'app/realtime/requests'
})

const apiReqs = io.counter({
    name: 'Total Request Count',
    id: 'app/realtime/requests'
})

const requestListener = function (req, res) {
    apiReqs.inc();
    apiTotalReqs.inc()

    var pathname=url.parse(req.url).pathname;

    switch(pathname){
        case "/summerServe.css":
            fs.readFile(__dirname + "/server/styles/summerServe.css")
                .then(contents => {
                    res.setHeader("Content-Type", "text/css");
                    res.writeHead(200);
                    res.end(contents);
                })
            break;
        case "/info":
            fs.readFile(__dirname + "/server/views/summerServeInfo.html")
                .then(contents => {
                    res.setHeader("Content-Type", "text/html");
                    res.writeHead(200);
                    res.end(contents);
                })
            break;
        case '/tram-api/status/text':
            res.end('Status: Online\nMaintenance: None Planned');
            break;
        default:
            fs.readFile(__dirname + "/server/views/summerServe.html")
                .then(contents => {
                    res.setHeader("Content-Type", "text/html");
                    res.writeHead(200);
                    res.end(contents);
                })
    }

    req.on('end', () => {
        apiReqs.dec()
    })
}

const server = http.createServer(requestListener);
server.listen(3000, 'localhost', () => {
    logger.info(`API: Running on port 3000`)
})